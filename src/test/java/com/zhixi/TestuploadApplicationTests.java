package com.zhixi;

import com.zhixi.pojo.MyUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@SpringBootTest
class TestuploadApplicationTests {

    @Test
    void contextLoads() throws IOException {
        //获取项目相对路径,上传文件到项目资源目录的upload下
        File directory = new File("src/main/resources/static/upload");
        String realPath = directory.getCanonicalPath();
        System.out.println("上传的文件夹是：" + realPath);
        // 获取文件夹下的所有文件
        File[] files = directory.listFiles();
        for (File file : files) {
            // 判断是否是文件夹
            if (file.isDirectory()) {
                System.out.println("文件夹名称：" + file.getName());
            }
            // 判断是否是文件
            if (file.isFile()) {
                System.out.println("文件名称：" + file.getName());
            }
        }
    }

    private MyUser user;

    @Autowired
    public TestuploadApplicationTests(MyUser user) {
        this.user = user;
    }

    @Test
    public void userTest(){
        System.out.println(this.user.getName());
        System.out.println(this.user.getAge());
        Map<String, Object> maps = this.user.getMaps();
        maps.forEach((k,v)->{
            System.out.println(k+"="+v);
        });
    }

}
